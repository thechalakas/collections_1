﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Collections_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //let me create a blocking collection
            //the blocking collection will allow only type of activity to happen at any given point of time
            //if an item is being added, then removal is not allowed
            //if an item is being removed, then adding is not allowed

            //a blocking collection of type string
            BlockingCollection<string> collection_blocks = new BlockingCollection<string>();
            List<string> activity = new List<string>();

            //one task for removal



            //one task for adding

            Task add_item = Task.Run(() =>
            {
                while(true)
                {
                    string entered_sentence = Console.ReadLine();
                    if(string.IsNullOrWhiteSpace(entered_sentence))
                    {
                        break;
                    }
                    collection_blocks.Add(entered_sentence);
                    activity.Add("addition done");
                    Console.WriteLine("{0} - added to the collection", entered_sentence);
                }
            }
                );

            Task remove_item = Task.Run(() =>
            {
                //I have an infinite 
                while (true)
                {

                        string temp = collection_blocks.Take();
                        Console.WriteLine(" {0} - is removed", temp);
                    activity.Add("removal done");
                }
            }
    );


            add_item.Wait();

            //let me display the collection

            int i = 0;
            foreach (string x in activity)
            {
                Console.WriteLine(" {0} - {1}", i++, x);
            }

            //stopping the console from dissapearing
            Console.ReadLine();
        }
    }
}
